# rigolds1000z

Rust wrapper around SCPI commands for Rigol's DS1000Z series oscilloscopes, tested on DS1054Z


Example:

```sh
env SCOPE_HOST="192.168.1.23:5555" cargo run --example idn
```

Warning: This is higly experimental
