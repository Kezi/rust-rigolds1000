use rigolds1000z::DS1000;

fn main() -> std::io::Result<()> {
    let mut scope = DS1000::new(&std::env::var("SCOPE_HOST").unwrap());

    println!("Scope id: {}", scope.idn()?);

    scope.stop()?; //stop to dump internal memory

    let samples = scope.get_waveform("CHAN1")?;

    println!("got {} samples", samples.len());

    Ok(())
}
