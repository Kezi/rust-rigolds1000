use rigolds1000z::DS1000;

fn main() -> std::io::Result<()> {
    let mut scope = DS1000::new(&std::env::var("SCOPE_HOST").unwrap());

    let id = scope.idn()?;
    println!("Scope id: {}", id);

    scope.send_command("*IDn?")?;
    let id_2 = scope.read_string()?;

    assert!(id == id_2);

    Ok(())
}
